<?php

//Deixar palavras em Maiuscula

$nome = 'paulo - teste teste';
echo strtoupper($nome);
echo'<hr>';

//Deixar palavras em Minusculas

echo strtolower($nome);
echo'<hr>';

//Deixar o Primeiro caracter em maiusculo
echo ucfirst($nome);
echo'<hr>';

//podemos usar uma função dentro da outra exe: Transforma em tudo minusculo e depois usar a função e transfora apenas a primeira letra em maiuscula

echo ucfirst(strtolower($nome));
echo'<hr>';

//Deixar cada  letra de uma palavra em maiusculo

echo ucwords(strtolower($nome));
echo'<hr>';

//---------------------------------------------

/*
função explode transforma uma string em array e implode transforma array em um string


*/

$info = 'São Paulo/SP/Brasil/Terra';

$info = explode('/' , $info);
var_dump($info);
echo $info[1]; 

echo'<hr>';

echo implode($info, ' * ');
echo'<hr>';

/*
Função que tira o espaço no começo

*/

$testes = ' Especializa';

var_dump($testes);
var_dump(ltrim($testes));

echo'<hr>';

$testes1 = ' Especializa ';

var_dump($testes1);
var_dump(rtrim($testes));
echo'<hr>';