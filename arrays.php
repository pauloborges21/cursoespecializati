<?php
//manipulação de arrays compact transformando variaveis em arrays

$nome = 'Carlos';
$companhia ='Rai';
$ano = '2018';



$infoCompany = compact('nome','companhia','ano');

var_dump($infoCompany);
echo'<hr>';

//verifica se é a variavel está recebendo um array


var_dump(is_array($infoCompany));
echo'<hr>';


//faz uma busca no array o primeiro parametro é o o que você procura o segundo é qual array que está a informação
var_dump(in_array('Carlos',$infoCompany));
echo'<hr>';

//para retornar as chaves do array

var_dump(array_keys($infoCompany));
echo'<hr>';
//para retornar somente o valores

var_dump(array_values($infoCompany));
echo'<hr>';
//retornar o valor da quantidade de atributos do array

var_dump(count($infoCompany));
echo'<hr>';
//juntando dois arrays 

$infoCompany2 = [
    'telefone'   => ['fixo','celular'
],
    'endereco'   => 'Rua Teste',
    'numero'     => '33',
    'estado'     => 'SP',

];


$merge = array_merge($infoCompany,$infoCompany2);

var_dump($merge);
echo'<hr>';

// removendo o ultimo elemento de um array 

$cart = [
'Arroz',
'Sabão',
'Feijão',
'Balinha',

];

var_dump($cart);
echo'<hr>';
array_pop($cart);

var_dump($cart);
echo'<hr>';

// removendo o primeiro elemento de um array 

array_shift($cart);
var_dump($cart);
echo'<hr>';

// eliminar uma variavel como um indice de um array

unset($cart[0]);
var_dump($cart);
echo'<hr>';

//adicionando um elemento no final do array

array_push($cart,'Tapete');
var_dump($cart);
echo'<hr>';

//Adicionando um elemento no inicio do array

array_unshift($cart, 'Leite');
array_push($cart,'Tapete');
var_dump($cart);
echo'<hr>';

//Remove Elementos duplicados do array

$removeDuplicados = array_unique($cart);
var_dump($removeDuplicados);

//Ordernação de Arrays Z para A decrescente o indice não é levado em consideração!!
$lista = [
0=> 'Macarrão',
1=> 'Feijão',
2=> 'Arroz',
3=> 'Batata',
4=> 'Pão',
];


arsort($lista);

var_dump($lista);

echo'<hr>';
//Ordernação de Arrays A para Z crescente o indice não é levado em consideração!!
asort($lista);

var_dump($lista);

echo'<hr>';

//Ordernação do indice----------------- Pelo que eu entendi primeiro fazemos a ordenação dos valores do array depois ordenamos os indices para uma melhor filtragem
sort($lista);

var_dump($lista);

echo'<hr>';

//pegando o ultimo elemento do array

echo $lista[count($lista)-1];  //Modo tradicional 
echo'<hr>';
echo end($lista);

echo'<hr>';


//Filtrar os valores do array

//precisamos criar uma função para retornar o que queremos!!

$idades = [
    35,
    18,
    12,
    15,
    14,
    27,
];

$idadesFiltrada =  array_filter($idades, function($idade) {
    return $idade >=18;

});
var_dump($idadesFiltrada);

echo'<hr>';


// Mapeando  
$nomes = [
    'carlos', 'pedro', 'João',
];


$nomes = array_map(function($nome){
    return strtoupper($nome);
}, $nomes

);
var_dump($nomes);

echo'<hr>';

//OUtro Jeito de fazer podemos criar a função separada e chamar a função 

$nomes = array_map('aplicandoLower',$nomes);

function aplicandoLower($valor){
    return strtolower($valor);
}
var_dump($nomes);

echo'<hr>';